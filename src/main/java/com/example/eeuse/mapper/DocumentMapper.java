package com.example.eeuse.mapper;


import com.example.eeuse.model.Document;
import org.dromara.easyes.core.core.BaseEsMapper;

/**
 * Mapper
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
public interface DocumentMapper extends BaseEsMapper<Document> {
}
